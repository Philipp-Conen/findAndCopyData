import os
import shutil, errno
import re
from pathlib import Path
from subprocess import call


#-----------------------------------

folderPathList = []

actualPath = os.getcwd()

filterFolder = 'AllTxts'

Path(actualPath+'/'+filterFolder).mkdir(parents=True, exist_ok=True)

#-----------------------------------

def findFilesAndCopy(actualPath,filterFolder):
	filenames_i_want = ["test.txt","test2.txt"]
	dest_dir = actualPath+'/'+filterFolder
	src_dir = actualPath
	#print(filenames_i_want)
	#print(actualPath+'/'+filterFolder)
	#print(actualPath)

	for (actualPath, dirnames, filenames) in os.walk(src_dir):
		for fname in filenames:
			if fname in filenames_i_want:
				print(os.path.join(actualPath, fname))
				shutil.copy(os.path.join(actualPath, fname), dest_dir)

def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise

def findFolderAndCopy(actualPath,filterFolder):
	foldernames_i_want = "/MeinOrdner" 
	dest_dir = actualPath+'/'+filterFolder
	src_dir = actualPath
	#print(filenames_i_want)
	#print(actualPath+'/'+filterFolder)
	#print(actualPath)

	for (actualPath, dirnames, filenames) in os.walk(src_dir):
		for dname in dirnames:
			if dname in foldernames_i_want:
				print(os.path.join(actualPath, dname))
				#shutil.copytree(os.path.join(actualPath, dname), dest_dir)
				#copyanything(src_dir, dest_dir)
				try:
					if os.path.exists(dest_dir):
						shutil.rmtree(dest_dir)
						shutil.copytree(src_dir, dest_dir)
				except OSError as e:
					if e.errno == errno.ENOTDIR:
						shutil.copy(source_dir_prompt, destination_dir_prompt)
					else:
						print('Directory not copied. Error: %s' % e)


def findFolderAndCopyNew(actualPath,filterFolder):
	#rePattern = "Mein.*"
	foldernames_i_want = ".txt" 
	dest_dir = actualPath+'/'+filterFolder
	src_dir = actualPath
	#print(filenames_i_want)
	#print(actualPath+'/'+filterFolder)
	#print(actualPath)

	for (actualPath, dirnames, filenames) in os.walk(src_dir):
		for dname in dirnames:
			#if dname in foldernames_i_want:
			if re.search(foldernames_i_want,dname):
				print(os.path.join(actualPath, dname))
				call(['cp', '-a', os.path.join(actualPath, dname), filterFolder])

#-----------------------------------


#findFilesAndCopy(actualPath,filterFolder)
findFolderAndCopyNew(actualPath,filterFolder)
